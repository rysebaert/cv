
## Infos {.unnumbered}

:::::::::::::: {.columns}
::: {.column width="65%"}

Je m’intéresse au cycle de vie de la donnée spatialisée : depuis sa collecte, son expertise, son traitement, son analyse et sa représentation. Résolument usager et contributeur de logiciels open source ainsi que des méthodes pour une science ouverte et reproductible, je promeus leur usage en dispensant formations et ateliers. Je partage et documente systématiquement le code source des analyses menées dans les différents projets dans lesquels je suis impliqué, principalement en R.

J'ai rejoint le [RIATE](https://riate.cnrs.fr/) en 2007, une Unité d'Appui et de Recherche du CNRS et de l'Université Paris Cité qui a pour mission d'élaborer des outils et des méthodes pour le traitement et la visualisation de données spatialisées.

Ce site Web rassemble et présente les principaux travaux auxquels j'ai participé ou j'ai été associé. 

:::

::: {.column width="5%"}

:::

::: {.column width="30%"}

#### Contact {.unnumbered}

- **Mail**: <a href="mailto:ronan.ysebaert@cnrs.fr" class="email">ronan.ysebaert@cnrs.fr</a>
- **GitHub**:  <a href="https://github.com/rysebaert">github.com/rysebaert</a>
- **GitLab**: <a href="https://gitlab.huma-num.fr/rysebaert">gitlab.huma-num.fr/rysebaert</a>
- **Tel**: +33 1 57 27 65 11.
- **ORCID**: [0000-0002-7344-5911](https://orcid.org/0000-0002-7344-5911)

::: 

::::::::::::::


:::::::::::::: {.columns}

::: {.column width="40%"}

### Parcours professionnel {.unnumbered}

**UAR RIATE** (2007 - Aujourd'hui)  
CNRS / Université Paris Cité - Paris, France.   

- Géomatique
- Analyse et visualisation de données spatialisées
- Harmonisation des données
- Science reproductible 
- Science ouverte
- Projets nationaux et internationaux 

**Nordregio** (2007)
Stockholm, Suède.

- Analyse de données spatialisées
- Cartographie
- Suivi de projet européen

### Formation

2002-2007 - Université Paris Diderot  
Master de géographie - [GAED GEOPRISME](https://mastergeoprisme.wordpress.com/){.external target="_blank"}
[Mémoire de M1](https://zenodo.org/records/10478508){.external target="_blank"}  / [Mémoire de M2](https://zenodo.org/records/10478478){.external target="_blank"} 
:::

::: {.column width="5%"}


:::

::: {.column width="25%"}

### Compétences {.unnumbered}

**Concepts**

- Géographie humaine
- Géographie de l'Europe, métropoles 

**Méthode**

- Statistiques 
- Analyse spatiale
- Géomatique
- Cartographie
- Sources de l'information géographique
- Qualité des données
- Principes, méthodes et outils pour la science ouverte
- Gestion de projets

:::

::: {.column width="25%"}

<br><br><br>

**Outils et logiciels**

- Logiciels de traitement de données et de programmation (R)
- Programmation lettrée (Quarto, RMardown)
- SIG, logiciels de cartographie thématique
- Outils et services de développement collaboratif et décentralisé (Git)

:::

::::::::::::::


