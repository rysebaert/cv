# Journées d'étude {.unnumbered}

- Laurian L., **Ysebaert R.**, Guérois M., Madelin M. (2023). [Exploiter les données Airbnb pour analyser la diffusion des locations touristiques de courte durée en région parisienne](https://urbaltour.hypotheses.org/1575){.external target="_blank"}. Séminaire Urbaltour : Les nouvelles données du web : acquisition et analyses, novembre 2023, Lyon.  
- Laurian L., **Ysebaert R.**, Guérois M., Madelin M. (2023). [Présentation d’une plateforme de visualisation du marché de type Airbnb et ses évolutions récentes en Île-de-France](https://riate-airbnb.gitpages.huma-num.fr/colloque/){.external target="_blank"}. Colloque RIATE - Atelier Parisien d’Urbanisme : Locations meublées touristiques dans le Grand Paris, septembre 2023, Paris. 
- **Ysebaert R.** (2023). [La programmation lettrée. Un paradigme à saisir pour l’ouverture et la reproductibilité des protocoles de recherche](https://rysebaert.gitpages.huma-num.fr/rdv_chn){.external target="_blank"}. Rendez-vous du Centre des Humanités Numériques, Université Paris Cité, avril 2023, Paris.
- **Ysebaert R.**, Lambert N. (2019). [Faire des cartes avec R, la frontière États-Unis - Mexique](https://riatelab.github.io/mexusaborder){.external target="_blank"}. Journée d’étude Geoteca, novembre 2019, Paris.
- Le Goix R., **Ysebaert R.**, Lieury M. (2019). [Le Big Data : quelles possibilités d’analyse des marchés immobiliers européens ?](https://zenodo.org/records/10246701){.external target="_blank"}. Journées NUMIMMO, septembre 2019, Avignon. 
- **Ysebaert R.**, Hadrossek C.  (2019). [Construire et partager ses jeux de données géographiques : Data Paper et entrepôts de données](https://hal.science/hal-03635191){.external target="_blank"}. Séminaire de l’équipe PARIS de l’UMR Géographie-cités, avril 2019, Paris.
- **Ysebaert R.**, Viry M. (2018). [Typologie socio-économique des régions de l’Union Européenne (2000-2012)](https://zenodo.org/records/10251340){.external target="_blank"}. Séminaire méthodologique de l’INSEE, mars 2018, Paris. 
- **Ysebaert R.**, Rosset C., Zanin C. (2018). ESPON et l’UMS RIATE, Point de contact – bases de données et outils pour l’analyse, rencontre des directeurs d’agence d’urbanisme, FNAU, janvier 2018, Paris. 
- **Ysebaert R.** (2016). HyperAtlas et HyperPolTerre. Séminaire Politiques de la Terre à l’épreuve de l’Anthropocène, Science-Po, mars 2016, Paris. 
- **Guérois M.**, Ysebaert R. (2015). Filling the data gap in border regions. 13th European Week of Regions and Cities, octobre 2015, Bruxelles, Belgique. 
- **Ysebaert R.** (2015). ESPON Database et HyperAtlas, Séminaire CGET, avril 2015, Paris. 
- **Ysebaert R.** (2015). Panorama de la « toolbox développée par ESPON, Séminaire ESPON 2020 – Enjeux européens de prospective territoriale. Conseil Régional de Picardie, mars 2015, Amiens. 
- **Ysebaert R.**, Guérois M., Feredj A., Viry M. (2015). Initiatives on cross border indicators – contributions from UMS RIATE-CGET, Developing Cross-border statistics, maison des provinces hollandaises, janvier 2015, Bruxelles, Belgique. 
- **Ysebaert R.** Le Rubrus B. (2014). La base de données ESPON et l’HyperAtlas, Séminaire ESPON 2020 – Enjeux européens de prospective territoriale, colloque « thématiques urbaines, scénarios et visions territoriales – la boîte à outils ESPON : en route vers EU2050, juin 2014, Bruxelles, Belgique. 
- **Ysebaert R.** (2011). Qu'attendre de la base de données ESPON ? Journées méthodologiques de l'INSEE, mars 2011, Paris.


