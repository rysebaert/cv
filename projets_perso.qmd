# Projets personnels {.unnumbered}

Des projets qui ont vacation à explorer des méthodes et des techniques utiles au traitement de l'information géographique. 


## Nepal expeditions viewer (2024)

:::::::::::::: {.columns}

::: {.column width="65%"}

Ce projet sans prétentions vise à explorer les potentialités offertes par la librairie ojs [geoviz](https://riatelab.github.io/geoviz/) et les [*dahsboards* Quarto](https://quarto.org/docs/dashboards/) avec le traitement original de la [Himalayan Database](http://www.himalayandatabase.com/), une base de données qui recense plus de 11300 expéditions dans l'Himalaya népalais depuis 1905. 

- [Page d'accueil du projet](https://rysebaert.github.io/nepal_expe/){.external target="_blank"}


:::

::: {.column width="2%"}

:::

::: {.column width="25%"}

<a href="https://rysebaert.github.io/nepal_expe/)" ><img src = "img/nepal.png" width="400"></a>

::: 

::::::::::::::


## Map templates (2024)

:::::::::::::: {.columns}

::: {.column width="65%"}

Ces notebooks présentent des solutions techniques pour créer des modèles cartographiques harmonieux pour l'Europe et la France en incluant les territoires périphériques dans l'emprise principale du modèle. Il s'agit ici de cas d'usage avancés de la librairie [mapinsetr](https://github.com/riatelab/mapinsetr). Pour la France, des polygones de Voronoi sont créés afin d'alléger les géométries initiales. Ces fonds de carte ainsi créés sont notamment utilisés dans l'application [Magrit](https://magrit.cnrs.fr/app/). 

- [Notebook Europe](https://rysebaert.github.io/map_template/){.external target="_blank"} - en français.
- [Notebook France](https://rysebaert.gitpages.huma-num.fr/fr_template/){.external target="_blank"} - en anglais.

::: 

::: {.column width="2%"}

:::

::: {.column width="25%"}

<a href="https://rysebaert.github.io/map_template/)" ><img src = "img/fig_template.png" width="400"></a>

::: 

::::::::::::::



## Climbing in Paris (2023)

:::::::::::::: {.columns}

::: {.column width="65%"}

Cette collection de notebooks présente comment construire, visualiser et reproduire des indicateurs d'accessibilité en combinant des points d'intérêt (POI) provenant d'OpenStreetMap, des indicateurs socio-économiques à l'échelle des IRIS provenant de sources institutionnelles (INSEE) et des engins de routage (OSRM). Le champ d'application est la pratique de l'escalade à Paris et sa proche périphérie en s'intéressant au temps d'accès en vélo aux Surfaces Artificielles d'Escalade (SAE)

- [Présentation du projet et des données mobilisées](https://observablehq.com/@rysebaert/forewords?collection=@rysebaert/climbing_paris){.external target="_blank"}
- [Analyse reproductible combinant du langage R et Ojs](https://rysebaert.github.io/climbing_paris/){.external target="_blank"}
- [Collection de visualisations](https://observablehq.com/collection/@rysebaert/climbing_paris){.external target="_blank"}

::: 

::: {.column width="2%"}

:::

::: {.column width="25%"}

<a href="https://rysebaert.github.io/climbing_paris/" ><img src = "img/climbing.jpg" width="400"></a>

::: 

::::::::::::::


## Maitron's cartographic visualization of worker's activism (2022)

:::::::::::::: {.columns}

::: {.column width="65%"}

Ce projet explore les possibilités offertes par la librairie bertin.js dans un Notebook Observable. Les données mobilisées proviennent du site [maitron-en-ligne](https://maitron.fr/spip.php?article140550) qui regroupe les biographies de plus 220 000 militants répertoriées dans le dictionnaire biographique du mouvement ouvrier français sur la période 1789-2000. On y retrouve des anarchistes, des cheminots, des enseignants, des fusillés et exécutés, des gaziers-électriciens, etc. Plusieurs représentations cartographiques interactives sont proposées pour comprendre la géographie de l'activité militante dans l'histoire récente française.   

- [Notebook Observable](https://observablehq.com/@rysebaert/maitrons-cartographic-visualization-of-workers-activism){.external target="_blank"} 

::: 

::: {.column width="2%"}

:::

::: {.column width="25%"}

<a href="https://observablehq.com/@rysebaert/maitrons-cartographic-visualization-of-workers-activism" ><img src = "img/maitron.png" width="400"></a>

::: 

::::::::::::::