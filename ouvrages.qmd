# Contribution à des ouvrages  {.unnumbered}

- Bennasr A., Grasland C., Nasr M., Yengui T., Zanin C., et al. (2022). [Atlas électoral tunisien](https://shs.hal.science/halshs-03821659){.external target="_blank"}. Centre d’Études et de Recherches Economiques et Sociales (CERES), 134 p.
- Lambert N., Giraud T., Ysebaert R. (2022). [Enjeux de communication dans la multireprésentation cartographique reproductible in Communication cartographique](https://cnrs.hal.science/hal-04130950){.external target="_blank"} *in* [Sémiologie graphique, sémiotique et visualisation](https://www.istegroup.com/fr/produit/communication-cartographique/){.external target="_blank"}. Mericskay B. (coord.). ISTE éditions, 264 p. {hal-04130950} 
- Migreurop (2022). [Atlas des migrations dans le Monde, Libertés de circulation, frontières inégalités](https://www.dunod.com/histoire-geographie-et-sciences-politiques/atlas-migrations-dans-monde-libertes-circulation){.external target="_blank"}. Armand Colin. 160 p. 
Grasland C., Guérois M., Ysebaert R. (2020). Border discontinuities *in* [Critical Dictionary on Cross Border Cooperation](https://www.peterlang.com/document/1057026){.external target="_blank"}. Wassenberg B. et Reitel B. (coord.). Peter Lang. 864 p. 
- Migreurop (2017). [Atlas des migrants en Europe, approche critique des politiques migratoires](https://www.dunod.com/histoire-geographie-et-sciences-politiques/atlas-migrants-en-europe-approches-critiques-politiques){.external target="_blank"}. Clochard O. (coord). Armand Colin. 176 p. 
- Ysebaert R., Guérois M. (2016). Les marges françaises en Europe : quelques clés de lecture cartographiques *in* [La France des Marges – Histoire – Géographie CAPES Agrégation](https://www.dunod.com/prepas-concours/france-marges-histoire-geographie-capes-agregation){.external target="_blank"}. Grésillon E. (coord .). Armand Colin. 448 p. 
- Baron M., Grasland C., Cunningham-Sabo E., Van Hamme G., Rivière D. (coord.) (2010). [Villes et régions Européennes en décroissance](https://www.lavoisier.fr/livre/genie-civil-BTP/villes-et-regions-europeennes-en-decroissance/baron/descriptif-9782746231108){.external target="_blank"}. Lavoisier. 346 p. 



