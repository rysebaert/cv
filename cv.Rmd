---
title: "Ronan Ysebaert CV"
author: Ronan Ysebaert
date: "`r Sys.Date()`"
output:
  pagedown::html_resume:
    # set it to true for a self-contained HTML page but it'll take longer to render
    self_contained: true
# uncomment this line to produce HTML and PDF in RStudio:
#knit: pagedown::chrome_print
---

```{css, echo = FALSE}
a {
  color: #c21f32
}
```

Aside
================================================================================


![Ronan Ysebert](img/avatar.PNG){width=80%}


Contact {#contact}
--------------------------------------------------------------------------------

- <i class="fa fa-envelope"></i> ronan.ysebaert@cnrs.fr
- <i class="fa fa-github"></i> [github.com/rysebaert](https://github.com/rysebaert)
- <i class="fa fa-gitlab"></i> [gitlab.huma-num.fr/rysebaert](https://gitlab.huma-num.fr/rysebaert)
- <i class="fa fa-phone"></i> +33 1 57 27 65 11


Compétences {#skills}
--------------------------------------------------------------------------------

- Analyse statistique, analyse spatiale, géomatique, cartographie

- Sources de l'information géographique et qualité des données

- Principes, méthodes et outils pour la science ouverte

- Gestion de projets

- Usage expert des logiciels d'analyse de données (R), de programmation lettrée (R Markdown, Quarto) et des méthodes de développement collaboratif et décentralisé (Git)


Disclaimer {#disclaimer}
--------------------------------------------------------------------------------

CV réalisé avec la librairie R [**pagedown**](https://github.com/rstudio/pagedown).

Denière mise à jour : `r Sys.Date()`.



Main
================================================================================

Ronan Ysebaert {#title}
--------------------------------------------------------------------------------

### Ingénieur d'études en sciences de l'information géographique


Professional Experience {data-icon=suitcase}
--------------------------------------------------------------------------------

### UAR RIATE

CNRS - Université Paris Cité

Paris, France

Aujourd'hui - 2007

::: concise
- Géomatique 
- Analyse et visualisation de données spatialisées
- Science reproductible
- Science ouverte
- Gestion de projets internationaux
:::

### Nordregio

N/A

Stockholm, Suède

2007

::: concise
- Analyse de données spatialisées
- Suivi de projet européen (Parlement Européen)
:::



Formation {data-icon=graduation-cap data-concise=true}
--------------------------------------------------------------------------------

### Université Paris Diderot

Master de géographie - [GAED - GEOPRISME](https://mastergeoprisme.wordpress.com/)

Paris, France

2007



Principaux projets de recherche {data-icon=laptop}
--------------------------------------------------------------------------------


### Horizon Europe - GRANULAR

Giving Rural Actors Novel data and re-Usable to Lead public Action in Rural areas

N/A

2026 - 2022

Coord. CIHEAM Montpellier, FR - 23 partenaires. 

::: concise
- Mise en oeuvre d'un cadre analytique dédié aux "nouvelles" données pour caractériser les espaces ruraux européens
- Participation Data Management Plan du projet
- Coordination des aspects méthodologiques pour le RIATE
::: 

### ANR WisDHoM

Inégalités patrimoniales et dynamiques du marché et du logement: Comprendre le Régime d'inégalités spatiales liées au marché de l'immobilier / [Site Web du projet](https://wisdhom.hypotheses.org/)

N/A

2022 - 2019

Coord. UMR Géographie-cités, FR, 4 partenaires


::: concise
- Coordination des réalisations statistiques et cartographiques des analyses harmonisées de l'ANR
- Réalisation d'un [site Web](https://rysebaert.gitpages.huma-num.fr/wisdhom-maps/index.html) pour restituer les résultats
::: 


### ESPON Housing Dynamics


Big Data for Territorial Analysis and Housing Dynamics / [Page de présentation du projet](https://www.espon.eu/big-data-housing)

N/A

2019 - 2018

Coord. UAR RIATE, FR, 4 partenaires

::: concise
- Management scientifique du projet
- Définition du protocole de récolte des données (Web scraping, données notariales et institutionnelles)
- Harmonisation (statistique, spatiale) des 10 cas d'étude du projet 
::: 

### ESPON 2020 MapKits

Collecte des fonds géographiques de référence et création des modèles cartographiques (R, QGIS, ArcGIS, Illustrator) du programme européen ESPON

N/A

2017 - 2016

Coord. UAR RIATE, FR, 2 partenaires

::: concise
- Coordination du projet
- Réalisation de 20 modèles cartographiques à des échelles géographiques variées
- Rédaction de documents méthodologiques pour la mise à jour et l'usage de ces ressources 
::: 

### ESPON Database 1 & ESPON M4D

Mise en oeuvre de la base de données du programme ESPON et création de nouvelles données pour l'observation territoriale 

N/A

2015 - 2008

Coord. UAR RIATE, FR, 6 partenaires (phase 1) puis 5 partenaires (phase 2)

::: concise
- Management scientifique de ces 2 projets (4 ans chacuns)
- Mobilisation des méthodes Agiles de gestion de projet
- Mise en conformité des données du programme à la directive INSPIRE. 
- Spécification et orientation du développement d'outils informatiques
- Production de documentation
- Création des protocoles d'estimation de données manquantes. 
::: 


Expérience d'enseignement {data-icon=chalkboard-teacher}
--------------------------------------------------------------------------------

### Introduction aux SIG et à la cartographie thématique

[Cours d'initiation](https://github.com/rysebaert/infogeo) destiné à des étudiants de Master non géographes

Université Paris Cité, France

2022 - 2014

### Sciences de l'Information Géographique Reproductible SIGR

[École thématique CNRS](https://sigr2021.github.io/site/index.html) destinée aux enseignants-chercheurs de l'ESR

Saint Pierre d'Oléron, France

2021


### Géovisualisation - Manipulation de données spatiales et cartographie

[Action Nationale de Formation Dataviz](https://riatelab.github.io/anfdataviz/) destinée aux ingénieurs et chercheurs du CNRS

Sète, France 

2018

Sélection de publications {data-icon=file}
--------------------------------------------------------------------------------

### Regioviz, un outil de géovisualisation pour situer les régions en Europe

*Mappemonde*, n°133, [Voir](https://journals.openedition.org/mappemonde/7498)

N/A

2022

Viry M, **Ysebaert R**, Guérois M



### Analyse Territorial Multiscalaire

Application à la concentration de l'emploi dans la métropole du Grand Paris, *Rzine*, [Voir](https://rzine.fr/docs/20211101_ysebaert_grasland_MTA/index.html) 

N/A 

2021 

**Ysebaert R**, Grasland C


### Unequal housing affordability across European cities

The ESPON Housing Database, Insights on Affordability in Selected Cities in Europe, *Cybergeo*, vol. 974, [Voir](https://doi.org/10.4000/cybergeo.36478) 

N/A

2021

Le Goix R, **Ysebaert R**, et al.


### Apports et limites des données OpenStreetMap pour l'analyse des centralités commerciales dans les espaces transfrontaliers

*Étude ANCT - RIATE*, [Accès à l'étude](https://hal.archives-ouvertes.fr/hal-03587399/file/RIATE_Rapport_OSM_Octobre2020.pdf}), [Accès au site Web](https://rcarto.gitpages.huma-num.fr/centralite/index.html)

N/A

2020

Guérois M, **Ysebaert R**, et al. 


### Disparities and territorial discontinuities in France with its new regions

A multiscalar and multidimensional interpretation, *Economie et Statistique / Economics and Statistics*, 497-498, 19-41, [Voir](https://doi.org/10.24187/ecostat.2017.497d.1928)

N/A

2017

Antunez K, Baccaïni B, Guérois M, **Ysebaert R**


### Map templates in a European Research program

Emerging consensus, without compromising cartographic innovation, *International Cartographic Conference*, [Voir](https://hal.archives-ouvertes.fr/hal-03591732/file/ica-proc-1-126-2018.pdf)

Washington, États-Unis

2017

Zanin C, Ysebaert R 



### Recueil, traçabilité et restitution des données territoriales du programme ESPON

*Mappemonde*, n°120, [Voir](https://doi.org/10.4000/mappemonde.2932)

N/A

2017

**Ysebaert R**, Salmon I, Le Rubrus B, Bernard C

### Typologie socio-économique des régions frontalières de l'UE - 2000-2012

*Étude ANCT - RIATE*, [Accès à l'étude](https://hal.archives-ouvertes.fr/hal-03587614/file/Typologie_socio_economique_des_regions_frontalieres_de-l-UE_2000_2012.pdf})

N/A

2016

Guérois M, **Ysebaert R**, et al. 

